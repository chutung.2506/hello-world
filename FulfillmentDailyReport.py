import urllib.request
import csv
import openpyxl
import pandas as pd
from datetime import timedelta
from openpyxl import load_workbook
from pandas.compat import StringIO

#GET QUERY
#XUAT - SAN LUONG
with urllib.request.urlopen("https://wfs-redash.ghn.vn/api/queries/295/results.csv?api_key=AnU0UCYqK6rHCsWgSarIoioF3rBryF0pBP7ABjsS") as url:
    San_luong_3months = url.read().decode()
#XUAT - LEADTIME
with urllib.request.urlopen("https://wfs-redash.ghn.vn/api/queries/304/results.csv?api_key=DIMs5r3vt6pAKZtB1bVBcHF8Lk012dwUwk6LhWII") as url:
    Leadtime_daily_3months = url.read().decode()
#XUAT - ONTIME
with urllib.request.urlopen(
        "https://wfs-redash.ghn.vn/api/queries/303/results.csv?api_key=Da6DZHEikZ30QZvZwxcDQ0buD2eU8fyTxkfQknxz") as url:
    Ontime_daily_3months = url.read().decode()
#XUAT - BACKLOG
with urllib.request.urlopen(
        "https://wfs-redash.ghn.vn/api/queries/300/results.csv?api_key=CTqj1PScbkBbmXQ6HjpK19NWyCljbMLpfcFfJtZl") as url:
    Backlog_aging = url.read().decode()

#NHAP - SAN LUONG
with urllib.request.urlopen(
        "https://wfs-redash.ghn.vn/api/queries/284/results.csv?api_key=j8or3JyvVevHE12SU74dYwIOdfdfCkb5Js1upjdV") as url:
    NhapNCC_sanluong_3months = url.read().decode()

#WRITE DATA TO EXCEL
#Read
San_luong_3months = pd.read_csv(StringIO(San_luong_3months))
Leadtime_daily_3months = pd.read_csv(StringIO(Leadtime_daily_3months))
Ontime_daily_3months = pd.read_csv(StringIO(Ontime_daily_3months))
Backlog_aging = pd.read_csv(StringIO(Backlog_aging))
#write to excel file
#XUAT - SAN LUONG
book = load_workbook('D:\Metrics WFS\Sanluong.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Sanluong.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
San_luong_3months.to_excel(writer, "San_luong_3months")
writer.save()
#XUAT - ONTIME
book = load_workbook('D:\Metrics WFS\Ontime1.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Ontime1.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
Ontime_daily_3months.to_excel(writer, "Ontime_daily_3months")
San_luong_3months.to_excel(writer, "San_luong_3months")
writer.save()
#XUAT - LEADTIME
book = load_workbook('D:\Metrics WFS\Leadtime.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Leadtime.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
Leadtime_daily_3months.to_excel(writer, "Leadtime_daily_3months")
San_luong_3months.to_excel(writer, "San_luong_3months")
writer.save()
#XUAT - BACKLOG
book = load_workbook('D:\Metrics WFS\Backlog.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Backlog.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
Backlog_aging.to_excel(writer, "Backlog_aging")
writer.save()
#
# #EXPORT PDF
from win32com import client
#XUAT - SAN LUONG
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Sanluong.xlsx')
ws = books.Worksheets['Report']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Sanluong.pdf')
books.Close()
xlApp.Quit()
#XUAT - ONTIME
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Ontime1.xlsx')
ws = books.Worksheets['Report']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Ontime.pdf')
books.Close()
xlApp.Quit()
#XUAT - LEADTIME
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Leadtime.xlsx')
ws = books.Worksheets['Report']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Leadtime.pdf')
books.Close()
xlApp.Quit()
#XUAT - BACKLOG
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Backlog.xlsx')
ws = books.Worksheets['Report']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Backlog.pdf')
books.Close()
xlApp.Quit()

#=====Phần này là METRICS nhập hàng===#
#GET QUERY
#NHAP NCC - SAN LUONG
with urllib.request.urlopen("https://wfs-redash.ghn.vn/api/queries/284/results.csv?api_key=j8or3JyvVevHE12SU74dYwIOdfdfCkb5Js1upjdV") as url:
    San_luong_nhapncc_3months = url.read().decode()
# #NHAP NCC - LEADTIME
with urllib.request.urlopen("https://wfs-redash.ghn.vn/api/queries/309/results.csv?api_key=8Be6QnhQxnxChJwi0HtxmVrMFb0QcpKSHx58bSB1") as url:
    Leadtime_nhapncc_daily_3months = url.read().decode()
#NHAP NCC - ONTIME
with urllib.request.urlopen("https://wfs-redash.ghn.vn/api/queries/310/results.csv?api_key=DH4iPhD0lAtldguo2SCKOzu6Kg2efRwDxMLwIB37") as url:
    Ontime_nhapncc_daily_3months = url.read().decode()
#NHAP NCC - SAN LUONG DON HOAN THANH, TINH CHO ONTIME & LEADTIME
with urllib.request.urlopen("https://wfs-redash.ghn.vn/api/queries/362/results.csv?api_key=r6sMKS3vJpZIPbzNVIsHuSE3DGNDYkbIfDuQRPMZ") as url:
    San_luong_nhapncc_3months_ontime_leadtime = url.read().decode()
#XUAT - BACKLOG
with urllib.request.urlopen(
        "https://wfs-redash.ghn.vn/api/queries/300/results.csv?api_key=CTqj1PScbkBbmXQ6HjpK19NWyCljbMLpfcFfJtZl") as url:
    Backlog_aging = url.read().decode()

#WRITE DATA TO EXCEL
#Read
San_luong_nhapncc_3months = pd.read_csv(StringIO(San_luong_nhapncc_3months))
Leadtime_nhapncc_daily_3months = pd.read_csv(StringIO(Leadtime_nhapncc_daily_3months))
Ontime_nhapncc_daily_3months = pd.read_csv(StringIO(Ontime_nhapncc_daily_3months))
San_luong_nhapncc_3months_ontime_leadtime = pd.read_csv(StringIO(San_luong_nhapncc_3months_ontime_leadtime))

#write to excel file
#NHAP - SAN LUONG
book = load_workbook('D:\Metrics WFS\Sanluong_NhapNCC.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Sanluong_NhapNCC.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
San_luong_nhapncc_3months.to_excel(writer, "San_luong_nhapncc_3months")
writer.save()
#NHAP - ONTIME
book = load_workbook('D:\Metrics WFS\Ontime_NhapNCC.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Ontime_NhapNCC.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
Ontime_nhapncc_daily_3months.to_excel(writer, "Ontime_nhapncc_daily_3months")
San_luong_nhapncc_3months_ontime_leadtime.to_excel(writer, "San_luong_nhapncc_3months")
writer.save()
#NHAP - LEADTIME
book = load_workbook('D:\Metrics WFS\Leadtime_NhapNCC.xlsx')
writer = pd.ExcelWriter('D:\Metrics WFS\Leadtime_NhapNCC.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
Leadtime_nhapncc_daily_3months.to_excel(writer, "Leadtime_nhapncc_daily_3months")
San_luong_nhapncc_3months_ontime_leadtime.to_excel(writer, "San_luong_nhapncc_3months")
writer.save()

from win32com import client
#NHAP - SAN LUONG
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Sanluong_NhapNCC.xlsx')
ws = books.Worksheets['Sheet1']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Sanluong_NhapNCC.pdf')
books.Close()
xlApp.Quit()

#NHAP - ONTIME
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Ontime_NhapNCC.xlsx')
ws = books.Worksheets['Sheet1']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Ontime_NhapNCC.pdf')
books.Close()
xlApp.Quit()

#NHAP - SAN LUONG
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Leadtime_NhapNCC.xlsx')
ws = books.Worksheets['Sheet1']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Leadtime_NhapNCC.pdf')
books.Close()
xlApp.Quit()


#====Phần này là Làm Mục Lục & Ghép file pdf====#

from win32com import client
#XUAT - BACKLOG
xlApp = client.Dispatch("Excel.Application")
books = xlApp.Workbooks.Open('D:\Metrics WFS\Mucluc.xlsx')
ws = books.Worksheets['Sheet1']
ws.Visible = 1
ws.ExportAsFixedFormat(0, 'D:\Metrics WFS\ Mucluc.pdf')
books.Close()
xlApp.Quit()

import time
TodaysDate = time.strftime("%Y%m%d")
reportfilename = TodaysDate +"_Fulfillment_DailyReport.pdf"

from PyPDF2 import PdfFileMerger
pdfs = ['D:\Metrics WFS\ Mucluc.pdf', 'D:\Metrics WFS\ Sanluong.pdf', 'D:\Metrics WFS\ Ontime.pdf', 'D:\Metrics WFS\ Leadtime.pdf', 'D:\Metrics WFS\ Backlog.pdf', 'D:\Metrics WFS\ Sanluong_NhapNCC.pdf', 'D:\Metrics WFS\ Ontime_NhapNCC.pdf', 'D:\Metrics WFS\ Leadtime_NhapNCC.pdf']
merger = PdfFileMerger()
for pdf in pdfs:
    merger.append(open(pdf, 'rb'))
with open('D:\Metrics WFS\ ' + reportfilename , 'wb') as fout:
    merger.write(fout)

#-----Phần này là send file bằng BOT lên Telegram-----------#
#Đặt tên file
import time
TodaysDate = time.strftime("%Y%m%d")
reportfilename = TodaysDate +"_Fulfillment_DailyReport.pdf"

import telegram
# Call bot
bot = telegram.Bot(token='502319545:AAHFSu5jbDB5RoK81qL2RjjLypfMBHrgWuw')
print(bot.get_me())

# Send telegram cho Group Test BOT
#bot.send_document(chat_id=-268483630, document=open('D:\Metrics WFS\ ' + reportfilename , 'rb'))

# Send telegram cho Chu Tùng
bot.send_document(chat_id=111961250, document=open('D:\Metrics WFS\ ' + reportfilename , 'rb'))

# Send telegram --> Fulfillment Group
# bot.send_document(chat_id=-161219372, document=open('D:\Metrics WFS\ ' + reportfilename , 'rb'))